# -*- mode: ruby -*-
# vi: set ft=ruby :

# set minimal Vagrant version
Vagrant.require_version ">= 1.8.5"

$script = <<SCRIPT
if which ansible >/dev/null; then
    echo '==============='
    echo 'ansible present'
    echo '==============='
else
    echo '=================================='
    echo 'update apt and upgrade base system'
    echo '=================================='
    sudo apt-get update
    sudo apt-get upgrade
    echo '==============='
    echo 'install ansible'
    echo '==============='
    sudo apt-get install -y lsb-core
    sudo apt-get -y install software-properties-common

    sudo apt-add-repository -y ppa:ansible/ansible
    sudo apt-get update
    sudo apt-get -y install ansible
    sudo apt-get -y install dos2unix
    cp /vagrant/provision/ansible/hosts /home/vagrant/
    chmod 666 /home/vagrant/hosts
    dos2unix /home/vagrant/hosts
fi
ansible --version
SCRIPT

$win_script = <<SCRIPT
sudo ansible-playbook /vagrant/provision/ansible/development.yml \
    -i /home/vagrant/hosts --connection=local \
    --extra-vars "project_root=/vagrant virtual_env_py3_root=/home/vagrant/.env3"
SCRIPT

Vagrant.configure(2) do |config|
  config.vm.box = "bento/ubuntu-16.04"
  config.vm.network "private_network", ip: "172.20.30.20"
  config.vm.network "forwarded_port", guest: 8000, host: 8000
  config.ssh.forward_agent = true
  # see https://twitter.com/mitchellh/status/525704126647128064
  config.ssh.insert_key = false

  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = false

    # Customize the amount of memory on the VM:
    vb.memory = "1024"
  end

  # Vagrant prefer to define hosts for Ansible this way
  # https://docs.vagrantup.com/v2/provisioning/ansible_intro.html
  config.vm.define "lightningwolf-sds-vm"

  # Install Ansible in the VM - this is needed for this tutorial only
  config.vm.provision "shell", inline: $script

  if Vagrant::Util::Platform.windows?
    # Provisioning on Windows environment
    config.vm.provision "shell", inline: $win_script
  else
    # Provisioning on Linux/Unix environment
    config.vm.provision :ansible do |ansible|
      ansible.playbook = 'provision/ansible/development.yml'

      # And we do not use provision/ansible/hosts for vagrant
      ansible.groups = {
        "development" => ["lightningwolf-sds-vm"],
        "development:vars" => {
          "project_root" => "/vagrant",
          "virtual_env_py3_root" => "/home/vagrant/.env3"
        }
      }

      #ask_sudo_pass = false
      #ansible.host_key_checking = false
      #ansible.extra_vars =

      #ansible.tags = []
      #ansible.skip_tags = ['']
      #ansible.verbose = 'vvvv'
      #ansible.raw_arguments = ['--check','--diff']
    end
  end

end
