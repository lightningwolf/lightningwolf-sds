.. _development-label:

===========
Development
===========

Project can be developed using `Ansible`_ or even `Ansible`_ + `Vagrant`_.
Base Provisioning scripts are prepared for `Ubuntu Server 16.04`_ and MacOS X with them please use:
:ref:`development-ansible-development-integration-label` otherwise use
:ref:`development-vagrant-integration-label`

For Mac OS X please read how to use pyenv:

*   https://gist.github.com/Bouke/11261620
*   http://www.holger-peters.de/using-pyenv-and-tox.html


.. _development-ansible-development-integration-label:

Option 1: Ansible Development Integration
-----------------------------------------

#.  `Ansible`_ must be installed:

    .. code-block:: bash

        sudo apt-get install -y software-properties-common
        sudo apt-add-repository -y ppa:ansible/ansible
        sudo apt-get update
        sudo apt-get install -y ansible

#.  Then just restart console and run command:

    .. code-block:: shell

        $ ansible-playbook provision/ansible/development.yml -i provision/ansible/hosts \
            -b --ask-become-pass \
            --extra-vars "project_root=`pwd`"

    In this moment we have all dependencies installed and prepared virtualenv
    environment for work.

#.  Please create your virtual environment and start working


.. _development-vagrant-integration-label:

Option 2: Vagrant Ansible Development Integration
-------------------------------------------------

#.  You can start vagrant machine:

    .. code-block:: bash

        $ vagrant up

    This will create your vagrant machine, run it and make it up and provision it.
    Vagrant is running provisioning only when we use this command first time.
    To run provision again we need to run command `vagrant provision`.

    .. note::

        Later you can always make ssh connection to your vagrant machine when
        machine is up:

        .. code-block:: shell

            $ vagrant ssh

        and run this playbook:

        .. code-block:: shell

            $ ansible-playbook /vagrant/provision/ansible/development.yml \
                -i /home/vagrant/hosts -b --ask-become-pass  \
                --extra-vars "project_root=/vagrant virtual_env_py3_root=/home/vagrant/.env3"

        or like suggested you can run vagrant provisioning when machine is up:

        .. code-block:: bash

            $ vagrant provision

#. Make ssh connection to your vagrant machine when machine is up:

    .. code-block:: shell

        $ vagrant ssh

    change directory to `/vagrant/lightningwolf-sds`:

    .. code-block:: shell

        $ cd /vagrant/lightningwolf-sds

#.  Please create your virtual environment and start working

Option 3: Own environment

#.  Please check required libraries by read role `provision/ansible/roles/common/python`


.. _Sphinx Doc: http://sphinx-doc.org/
.. _Ansible: http://docs.ansible.com/
.. _Vagrant: https://www.vagrantup.com/
.. _Ubuntu Server 16.04: http://www.ubuntu.com/download/server
