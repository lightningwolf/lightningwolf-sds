#!/usr/bin/env python
# -*- coding: utf-8 -*-
import uuid

from pip.req import parse_requirements
from setuptools import find_packages, setup


def parse_requirements_file(path):
    # parse_requirements() returns generator
    # of pip.req.InstallRequirement objects
    req = parse_requirements(path, session=uuid.uuid1())
    return [str(ir.req) for ir in req]

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read().replace(
        '.. :changelog:', ''
    )

requirements = parse_requirements_file('requirements/main.txt')

test_requirements = parse_requirements_file('requirements/test.txt')

setup(
    name='lightningwolf_sds',
    version='0.1.0',
    description="Lightningwolf Simple DataStore",
    long_description=readme + '\n\n' + history,
    author="Arkadiusz Tułodziecki",
    author_email='atulodzi@gmail.com',
    url='https://gitlab.com/lightningwolf/lightningwolf_sds',
    packages=find_packages(),
    package_dir={'lightningwolf_sds':
                 'lightningwolf_sds'},
    entry_points={
        'console_scripts': [
            'lightningwolf_sds=lightningwolf_sds.cli:main'
        ]
    },
    include_package_data=True,
    install_requires=requirements,
    license="MIT license",
    zip_safe=False,
    keywords='lightningwolf_sds',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    test_suite='tests',
    tests_require=test_requirements
)
