#!/bin/bash
ansible-playbook provision/ansible/development.yml -i provision/ansible/hosts \
    --extra-vars "project_root=`pwd` virtual_env_py3_root=`pwd`/.env3" --ask-become
