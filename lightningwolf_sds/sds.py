#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lightningwolf_sds.core.factory import create_app

__author__ = u'Arkadiusz Tułodziecki'
__email__ = 'atulodzi@gmail.com'

app = create_app()


@app.route('/')
def hello_world():
    return 'Hello World!'


if __name__ == '__main__':
    app.run(host='0.0.0.0')
