#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Blueprint
from lightningwolf_sds.core.errors import SdsNotImplemented

# Blueprint declaration
bucket = Blueprint('bucket', __name__, subdomain="<bucket_name>")


# Add a route to the blueprint
@bucket.route("/", methods=['PUT'])
def create(bucket_name):
    raise SdsNotImplemented(message='A header you provided implies functionality that is not implemented.')
    # length = request.headers.get('Content-Length')
    # date = request.headers.get('Date')
    # authorization_string = request.headers.get('Authorization')
    # return jsonify(
    #     {'bucket_name': bucket_name, 'length': length, 'date': date, 'authorization_string': authorization_string}
    # )
    # pass
