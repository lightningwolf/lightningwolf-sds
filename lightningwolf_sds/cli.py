# -*- coding: utf-8 -*-
# See click documentation at http://click.pocoo.org/

import click
from lightningwolf_sds.sds import app


@click.command()
def main(args=None):
    """Console script for lightningwolf_sds"""
    click.echo(
        "Starting service"
    )
    app.run(host='0.0.0.0')


if __name__ == "__main__":
    main()
