# -*- coding: utf-8 -*-

http_statuses = {
    301: 'Moved Permanently',
    307: 'Moved Temporarily',
    400: 'Bad Request',
    401: 'Unauthorized',
    403: 'Forbidden',
    404: 'Not Found',
    405: 'Method Not Allowed',
    409: 'Conflict',
    411: 'Length Required',
    412: 'Precondition Failed',
    416: 'Requested Range Not Satisfiable',
    429: 'Too Many Requests',
    500: 'Internal Server Error',
    501: 'Not Implemented',
    503: 'Service Unavailable'
}


class CoreException(Exception):
    def __init__(self, code, message, request_id, resource, status_code):
        """
            Initialising Core Exception

            :param  code:       The error code is a string that uniquely identifies an error condition. It is meant to
                                be read and understood by programs that detect and handle errors by type. For more
                                information, see AWS S3 Documentation - List of Error Codes (p. 8).
                                Type: String

            :param message:     The error message contains a generic description of the error condition in English. It
                                is intended for a human audience. Simple programs display the message directly to
                                the end user if they encounter an error condition they don't know how or don't care
                                to handle. Sophisticated programs with more exhaustive error handling and proper
                                internationalization are more likely to ignore the error message.
                                Type: String

            :param request_id:  ID of the request associated with the error.
                                Type: String

            :param resource:    The bucket or object that is involved in the error.
                                Type: String
            :param status_code: HTTP Status Code.
                                Type: Integer
        """
        Exception.__init__(self)
        self.__code = code
        self.__message = message
        self.__request_id = request_id
        self.__resource = resource
        self.__status_code = status_code

    def get_code(self):
        return self.__code

    def get_message(self):
        return self.__message

    def get_request_id(self):
        return self.__request_id

    def get_resource(self):
        return self.__resource

    def get_status_code(self):
        return self.__status_code

    code = property(get_code)
    message = property(get_message)
    request_id = property(get_request_id)
    resource = property(get_resource)
    status_code = property(get_status_code)


class SdsAccessDenied(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Access Denied

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="AccessDenied",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=403
        )


class SdsAccountProblem(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        There is a problem with your lightningwolf-sds account that prevents the operation from completing successfully.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="AccountProblem",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=403
        )


class SdsAmbiguousGrantByEmailAddress(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The email address you provided is associated with more than one account.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="AmbiguousGrantByEmailAddress",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsBadDigest(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The Content-MD5 you specified did not match what we received.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="BadDigest",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsBucketAlreadyExists(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The requested bucket name is not available. The bucket namespace is shared by all users of the system.
        Please select a different name and try again.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="BucketAlreadyExists",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=409
        )


class SdsBucketAlreadyOwnedByYou(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Your previous request to create the named bucket succeeded and you already own it. You get this error in all
        AWS regions except US East (N. Virginia) region, us-east-1. In us-east-1 region, you will get 200 OK, but it is
        no-op (if bucket exists it Amazon S3 will not do anything).

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="BucketAlreadyOwnedByYou",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=409
        )


class SdsBucketNotEmpty(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The bucket you tried to delete is not empty.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="BucketNotEmpty",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=409
        )


class SdsCredentialsNotSupported(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        This request does not support credentials.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="CredentialsNotSupported",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsCrossLocationLoggingProhibited(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Cross-location logging not allowed. Buckets in one geographic location cannot log information to a bucket in
        another location.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="CrossLocationLoggingProhibited",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=403
        )


class SdsEntityTooSmall(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Your proposed upload is smaller than the minimum allowed object size.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="EntityTooSmall",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsEntityTooLarge(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Your proposed upload exceeds the maximum allowed object size.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="EntityTooLarge",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsExpiredToken(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The provided token has expired.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="ExpiredToken",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsIllegalVersioningConfigurationException(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Indicates that the versioning configuration specified in the request is invalid.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="IllegalVersioningConfigurationException",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsIncompleteBody(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        You did not provide the number of bytes specified by the Content-Length Request HTTP header

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="IncompleteBody",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsIncorrectNumberOfFilesInPostRequest(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        POST requires exactly one file upload per request.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="IncorrectNumberOfFilesInPostRequest",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsInlineDataTooLarge(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Inline data exceeds the maximum allowed size.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InlineDataTooLarge",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsInternalError(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        We encountered an internal error. Please try again.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InternalError",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=500
        )


class SdsInvalidAccessKeyId(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The AWS access key Id you provided does not exist in our records.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidAccessKeyId",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=403
        )


class SdsInvalidAddressingHeader(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        You must specify the Anonymous role.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidAddressingHeader",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=None
        )


class SdsInvalidArgument(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Invalid Argument

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidArgument",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsInvalidBucketName(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The specified bucket is not valid.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidBucketName",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsInvalidBucketState(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The request is not valid with the current state of the bucket.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidBucketState",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=409
        )


class SdsInvalidDigest(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The Content-MD5 you specified is not valid.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidDigest",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsInvalidEncryptionAlgorithmError(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The encryption request you specified is not valid. The valid value is AES256.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidEncryptionAlgorithmError",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsInvalidLocationConstraint(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The specified location constraint is not valid.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidLocationConstraint",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsInvalidObjectState(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The operation is not valid for the current state of the object.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidObjectState",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=403
        )


class SdsInvalidPart(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        One or more of the specified parts could not be found. The part might not Request have been uploaded,
        or the specified entity tag might not have matched the part's entity tag.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidPart",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsInvalidPartOrder(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The list of parts was not in ascending order.Parts list must specified in order Request by part number.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidPartOrder",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsInvalidPayer(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        All access to this object has been disabled.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidPayer",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=403
        )


class SdsInvalidPolicyDocument(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The content of the form does not meet the conditions specified in the policy Request document.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidPolicyDocument",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsInvalidRange(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The requested range cannot be satisfied.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidRange",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=416
        )


class SdsInvalidRequest(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        One of:
            - Please use AWS4-HMAC-SHA256.
            - SOAP requests must be made over an HTTPS connection.
            - S3 Transfer Acceleration is not supported for buckets with non-DNS compliant names.
            - S3 Transfer Acceleration is not supported for buckets with periods (.) in their names.
            - S3 Transfer Accelerate endpoint only supports virtual style requests.
            - S3 Transfer Accelerate is not configured on this bucket.
            - S3 Transfer Accelerate is disabled on this bucket.
            - S3 Transfer Acceleration is not supported on this bucket. Contact AWS Support for more information.
            - S3 Transfer Acceleration cannot be enabled on this bucket. Contact AWS Support for more information.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidRequest",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsInvalidSecurity(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The provided security credentials are not valid.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidSecurity",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=403
        )


class SdsInvalidSOAPRequest(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The SOAP request body is invalid.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidSOAPRequest",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsInvalidStorageClass(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The storage class you specified is not valid.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidStorageClass",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsInvalidTargetBucketForLogging(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The target bucket for logging does not exist, is not owned by you, or does not have the appropriate
        grants for the log-delivery group.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidTargetBucketForLogging",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsInvalidToken(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The provided token is malformed or otherwise invalid.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidToken",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsInvalidURI(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Couldn't parse the specified URI.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="InvalidURI",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsKeyTooLong(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Your key is too long.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="KeyTooLong",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsMalformedACLError(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The XML you provided was not well-formed or did not validate against our published schema.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="MalformedACLError",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsMalformedPOSTRequest(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The body of your POST request is not well-formed multipart/form-data.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="MalformedPOSTRequest",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsMalformedXML(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        This happens when the user sends malformed xml (xml that doesn't conform to the published xsd) for
        the configuration. The error message is:
            "The XML you provided was not well-formed or did not validate against our published schema."

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="MalformedXML",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsMaxMessageLengthExceeded(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Your request was too big.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="MaxMessageLengthExceeded",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsMaxPostPreDataLengthExceededError(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Your POST request fields preceding the upload file were too large.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="MaxPostPreDataLengthExceededError",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsMetadataTooLarge(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Your metadata headers exceed the maximum allowed metadata size.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="MetadataTooLarge",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsMethodNotAllowed(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The specified method is not allowed against this resource.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="MethodNotAllowed",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=405
        )


class SdsMissingAttachment(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        A SOAP attachment was expected, but none were found.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="MissingAttachment",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=None
        )


class SdsMissingContentLength(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        You must provide the Content-Length HTTP header.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="MissingContentLength",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=411
        )


class SdsMissingRequestBodyError(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        This happens when the user sends an empty xml document as a request.
        The error message is, "Request body is empty."

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="MissingRequestBodyError",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsMissingSecurityElement(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The SOAP 1.1 request is missing a security element.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="MissingSecurityElement",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsMissingSecurityHeader(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Your request is missing a required header.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="MissingSecurityHeader",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsNoLoggingStatusForKey(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        There is no such thing as a logging status sub-resource for a key.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="NoLoggingStatusForKey",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsNoSuchBucket(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The specified bucket does not exist.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="NoSuchBucket",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=404
        )


class SdsNoSuchKey(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The specified key does not exist.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="NoSuchKey",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=404
        )


class SdsNoSuchLifecycleConfiguration(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The lifecycle configuration does not exist.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="NoSuchLifecycleConfiguration",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=404
        )


class SdsNoSuchUpload(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The specified multipart upload does not exist.The upload ID might be invalid, or the multipart upload might have
        been aborted or completed.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="NoSuchUpload",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=404
        )


class SdsNoSuchVersion(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Indicates that the version ID specified in the request does not match an existing version.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="NoSuchVersion",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=404
        )


class SdsNotImplemented(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        A header you provided implies functionality that is not implemented.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="NotImplemented",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=501
        )


class SdsNotSignedUp(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Your account is not signed up for the SDS S3 service. You must sign up before you can use SDS S3.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="NotSignedUp",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=403
        )


class SdsNoSuchBucketPolicy(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The specified bucket does not have a bucket policy.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="NoSuchBucketPolicy",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=404
        )


class SdsOperationAborted(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        A conflicting conditional operation is currently in progress against this resource. Try again.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="OperationAborted",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=409
        )


class SdsPermanentRedirect(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The bucket you are attempting to access must be addressed using the specified endpoint. Send all future requests
        to this endpoint.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="PermanentRedirect",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=301
        )


class SdsPreconditionFailed(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        At least one of the preconditions you specified did not hold.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="PreconditionFailed",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=412
        )


class SdsRedirect(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Temporary redirect.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="Redirect",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=307
        )


class SdsRestoreAlreadyInProgress(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Object restore is already in progress.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="RestoreAlreadyInProgress",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=409
        )


class SdsRequestIsNotMultiPartContent(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Bucket POST must be of the enclosure-type multipart/form-data.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="RequestIsNotMultiPartContent",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsRequestTimeout(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Your socket connection to the server was not read from or written to within the timeout period.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="RequestTimeout",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsRequestTimeTooSkewed(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The difference between the request time and the server's time is too large.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="RequestTimeTooSkewed",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=403
        )


class SdsRequestTorrentOfBucketError(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Requesting the torrent file of a bucket is not permitted.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="RequestTorrentOfBucketError",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsSignatureDoesNotMatch(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The request signature we calculated does not match the signature you provided. Check your SDS secret access key
        and signing method. For more information, see REST Authentication and SOAP Authentication for details.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="SignatureDoesNotMatch",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=403
        )


class SdsServiceUnavailable(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Reduce your request rate.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="ServiceUnavailable",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=503
        )


class SdsSlowDown(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        Reduce your request rate.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="SlowDown",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=503
        )


class SdsTemporaryRedirect(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        You are being redirected to the bucket while DNS updates.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="TemporaryRedirect",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=307
        )


class SdsTokenRefreshRequired(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The provided token must be refreshed.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="TokenRefreshRequired",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsTooManyBuckets(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        You have attempted to create more buckets than allowed.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="TooManyBuckets",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsUnexpectedContent(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        This request does not support content.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="UnexpectedContent",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsUnresolvableGrantByEmailAddress(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The email address you provided does not match any account on record.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="UnresolvableGrantByEmailAddress",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )


class SdsUserKeyMustBeSpecified(CoreException):
    def __init__(self, message, request_id="Unknown", resource="Unknown"):
        """
        The bucket POST must contain the specified field name. If it is specified, check the order of the fields.

        :param message:         See @CoreException
        :param request_id:      See @CoreException
        :param resource:        See @CoreException
        """
        CoreException.__init__(
            self,
            code="UserKeyMustBeSpecified",
            message=message,
            request_id=request_id,
            resource=resource,
            status_code=400
        )
