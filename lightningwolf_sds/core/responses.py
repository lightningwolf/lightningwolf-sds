#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Response
from lxml import etree


def error_response(e):
    root = etree.Element("Error")
    etree.SubElement(root, "Code").text = e.get_code()
    etree.SubElement(root, "Message").text = e.get_message()
    etree.SubElement(root, "Resource").text = e.get_resource()
    etree.SubElement(root, "RequestId").text = e.get_request_id()
    xml = etree.tostring(root, pretty_print=False)
    return Response(
        response=u'<?xml version="1.0" encoding="UTF-8"?>{}'.format(xml.decode("utf-8")),
        status=e.get_status_code(),
        mimetype='application/xml'
    )
