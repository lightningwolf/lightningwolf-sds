#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask
from lightningwolf_sds.core.errors import CoreException
from lightningwolf_sds.core.responses import error_response


def create_app():
    app = Flask(__name__)
    app.config['SERVER_NAME'] = 'dev-sds.lightningwolf.pl:5000'
    app.config['DEBUG'] = True
    # app.config.from_pyfile(config_filename)
    app.port = 5000

    # Configure Error Handler
    error_handler(app=app)

    # from yourapplication.model import db
    # db.init_app(app)

    from lightningwolf_sds.views.bucket import bucket
    app.register_blueprint(bucket)

    return app


def error_handler(app):
    @app.errorhandler(CoreException)
    def handle_core_exception(e):
        return error_response(e)
