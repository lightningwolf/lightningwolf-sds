#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import url_for


def test_put_bucket_not_implemented(client):
    assert client.put(url_for('bucket.create', bucket_name='my-bucket')).status_code == 501
