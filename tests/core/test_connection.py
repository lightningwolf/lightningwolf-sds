# -*- coding: utf-8 -*-
from boto.s3.connection import S3Connection
from lightningwolf_sds.core.connection import SDSConnection


def test_for_subclass():
    assert issubclass(SDSConnection, S3Connection), 'Ensure that our SDSConnection is a subclass of S3Connection'
