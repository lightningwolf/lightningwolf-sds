# -*- coding: utf-8 -*-

import pytest

from lightningwolf_sds.core.errors import (CoreException, SdsAccessDenied,
                                           SdsAccountProblem,
                                           SdsAmbiguousGrantByEmailAddress,
                                           SdsBadDigest,
                                           SdsBucketAlreadyExists,
                                           SdsBucketAlreadyOwnedByYou,
                                           SdsBucketNotEmpty,
                                           SdsCredentialsNotSupported,
                                           SdsCrossLocationLoggingProhibited,
                                           SdsEntityTooLarge,
                                           SdsEntityTooSmall, SdsExpiredToken,
                                           SdsIllegalVersioningConfigurationException,
                                           SdsIncompleteBody,
                                           SdsIncorrectNumberOfFilesInPostRequest,
                                           SdsInlineDataTooLarge,
                                           SdsInternalError,
                                           SdsInvalidAccessKeyId,
                                           SdsInvalidAddressingHeader,
                                           SdsInvalidArgument,
                                           SdsInvalidBucketName,
                                           SdsInvalidBucketState,
                                           SdsInvalidDigest,
                                           SdsInvalidEncryptionAlgorithmError,
                                           SdsInvalidLocationConstraint,
                                           SdsInvalidObjectState,
                                           SdsInvalidPart, SdsInvalidPartOrder,
                                           SdsInvalidPayer,
                                           SdsInvalidPolicyDocument,
                                           SdsInvalidRange, SdsInvalidRequest,
                                           SdsInvalidSecurity,
                                           SdsInvalidSOAPRequest,
                                           SdsInvalidStorageClass,
                                           SdsInvalidTargetBucketForLogging,
                                           SdsInvalidToken, SdsInvalidURI,
                                           SdsKeyTooLong, SdsMalformedACLError,
                                           SdsMalformedPOSTRequest,
                                           SdsMalformedXML,
                                           SdsMaxMessageLengthExceeded,
                                           SdsMaxPostPreDataLengthExceededError,
                                           SdsMetadataTooLarge,
                                           SdsMethodNotAllowed,
                                           SdsMissingAttachment,
                                           SdsMissingContentLength,
                                           SdsMissingRequestBodyError,
                                           SdsMissingSecurityElement,
                                           SdsMissingSecurityHeader,
                                           SdsNoLoggingStatusForKey,
                                           SdsNoSuchBucket,
                                           SdsNoSuchBucketPolicy, SdsNoSuchKey,
                                           SdsNoSuchLifecycleConfiguration,
                                           SdsNoSuchUpload, SdsNoSuchVersion,
                                           SdsNotImplemented, SdsNotSignedUp,
                                           SdsOperationAborted,
                                           SdsPermanentRedirect,
                                           SdsPreconditionFailed, SdsRedirect,
                                           SdsRequestIsNotMultiPartContent,
                                           SdsRequestTimeout,
                                           SdsRequestTimeTooSkewed,
                                           SdsRequestTorrentOfBucketError,
                                           SdsRestoreAlreadyInProgress,
                                           SdsServiceUnavailable,
                                           SdsSignatureDoesNotMatch,
                                           SdsSlowDown, SdsTemporaryRedirect,
                                           SdsTokenRefreshRequired,
                                           SdsTooManyBuckets,
                                           SdsUnexpectedContent,
                                           SdsUnresolvableGrantByEmailAddress,
                                           SdsUserKeyMustBeSpecified)


def test_core_unknown_exception():
    with pytest.raises(CoreException) as exc_info:
        raise CoreException(
            code="UnknownCode",
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource",
            status_code=503
        )
    assert exc_info.value.code == "UnknownCode"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 503


def test_access_denied_exception():
    with pytest.raises(SdsAccessDenied) as exc_info:
        raise SdsAccessDenied(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "AccessDenied"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 403


def test_account_problem_exception():
    with pytest.raises(SdsAccountProblem) as exc_info:
        raise SdsAccountProblem(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "AccountProblem"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 403


def test_ambiguous_grant_by_email_address_exception():
    with pytest.raises(SdsAmbiguousGrantByEmailAddress) as exc_info:
        raise SdsAmbiguousGrantByEmailAddress(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "AmbiguousGrantByEmailAddress"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_bad_digest_exception():
    with pytest.raises(SdsBadDigest) as exc_info:
        raise SdsBadDigest(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "BadDigest"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_bucket_already_exists_exception():
    with pytest.raises(SdsBucketAlreadyExists) as exc_info:
        raise SdsBucketAlreadyExists(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "BucketAlreadyExists"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 409


def test_bucket_already_owned_by_you_exception():
    with pytest.raises(SdsBucketAlreadyOwnedByYou) as exc_info:
        raise SdsBucketAlreadyOwnedByYou(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "BucketAlreadyOwnedByYou"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 409


def test_bucket_not_empty_exception():
    with pytest.raises(SdsBucketNotEmpty) as exc_info:
        raise SdsBucketNotEmpty(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "BucketNotEmpty"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 409


def test_credentials_not_supported_exception():
    with pytest.raises(SdsCredentialsNotSupported) as exc_info:
        raise SdsCredentialsNotSupported(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "CredentialsNotSupported"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_cross_location_logging_prohibited_exception():
    with pytest.raises(SdsCrossLocationLoggingProhibited) as exc_info:
        raise SdsCrossLocationLoggingProhibited(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "CrossLocationLoggingProhibited"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 403


def test_entity_too_small_exception():
    with pytest.raises(SdsEntityTooSmall) as exc_info:
        raise SdsEntityTooSmall(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "EntityTooSmall"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_entity_too_large_exception():
    with pytest.raises(SdsEntityTooLarge) as exc_info:
        raise SdsEntityTooLarge(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "EntityTooLarge"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_expired_token_exception():
    with pytest.raises(SdsExpiredToken) as exc_info:
        raise SdsExpiredToken(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "ExpiredToken"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_illegal_versioning_configuration_exception():
    with pytest.raises(SdsIllegalVersioningConfigurationException) as exc_info:
        raise SdsIllegalVersioningConfigurationException(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "IllegalVersioningConfigurationException"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_incomplete_body_exception():
    with pytest.raises(SdsIncompleteBody) as exc_info:
        raise SdsIncompleteBody(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "IncompleteBody"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_incorrect_number_of_files_in_post_request_exception():
    with pytest.raises(SdsIncorrectNumberOfFilesInPostRequest) as exc_info:
        raise SdsIncorrectNumberOfFilesInPostRequest(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "IncorrectNumberOfFilesInPostRequest"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_inline_data_too_large_exception():
    with pytest.raises(SdsInlineDataTooLarge) as exc_info:
        raise SdsInlineDataTooLarge(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InlineDataTooLarge"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_internal_error_exception():
    with pytest.raises(SdsInternalError) as exc_info:
        raise SdsInternalError(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InternalError"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 500


def test_invalid_access_key_id_exception():
    with pytest.raises(SdsInvalidAccessKeyId) as exc_info:
        raise SdsInvalidAccessKeyId(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidAccessKeyId"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 403


def test_invalid_addressing_header_exception():
    with pytest.raises(SdsInvalidAddressingHeader) as exc_info:
        raise SdsInvalidAddressingHeader(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidAddressingHeader"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code is None


def test_invalid_argument_exception():
    with pytest.raises(SdsInvalidArgument) as exc_info:
        raise SdsInvalidArgument(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidArgument"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_invalid_bucket_name_exception():
    with pytest.raises(SdsInvalidBucketName) as exc_info:
        raise SdsInvalidBucketName(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidBucketName"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_invalid_bucket_state_exception():
    with pytest.raises(SdsInvalidBucketState) as exc_info:
        raise SdsInvalidBucketState(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidBucketState"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 409


def test_invalid_digest_exception():
    with pytest.raises(SdsInvalidDigest) as exc_info:
        raise SdsInvalidDigest(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidDigest"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_invalid_encryption_algorithm_error_exception():
    with pytest.raises(SdsInvalidEncryptionAlgorithmError) as exc_info:
        raise SdsInvalidEncryptionAlgorithmError(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidEncryptionAlgorithmError"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_invalid_location_constraint_exception():
    with pytest.raises(SdsInvalidLocationConstraint) as exc_info:
        raise SdsInvalidLocationConstraint(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidLocationConstraint"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_invalid_object_state_exception():
    with pytest.raises(SdsInvalidObjectState) as exc_info:
        raise SdsInvalidObjectState(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidObjectState"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 403


def test_invalid_part_exception():
    with pytest.raises(SdsInvalidPart) as exc_info:
        raise SdsInvalidPart(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidPart"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_invalid_part_order_exception():
    with pytest.raises(SdsInvalidPartOrder) as exc_info:
        raise SdsInvalidPartOrder(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidPartOrder"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_invalid_payer_exception():
    with pytest.raises(SdsInvalidPayer) as exc_info:
        raise SdsInvalidPayer(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidPayer"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 403


def test_invalid_policy_document_exception():
    with pytest.raises(SdsInvalidPolicyDocument) as exc_info:
        raise SdsInvalidPolicyDocument(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidPolicyDocument"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_invalid_range_exception():
    with pytest.raises(SdsInvalidRange) as exc_info:
        raise SdsInvalidRange(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidRange"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 416


def test_invalid_request_exception():
    with pytest.raises(SdsInvalidRequest) as exc_info:
        raise SdsInvalidRequest(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidRequest"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_invalid_security_exception():
    with pytest.raises(SdsInvalidSecurity) as exc_info:
        raise SdsInvalidSecurity(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidSecurity"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 403


def test_invalid_soap_request_exception():
    with pytest.raises(SdsInvalidSOAPRequest) as exc_info:
        raise SdsInvalidSOAPRequest(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidSOAPRequest"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_invalid_storage_class_exception():
    with pytest.raises(SdsInvalidStorageClass) as exc_info:
        raise SdsInvalidStorageClass(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidStorageClass"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_invalid_target_bucket_for_logging_exception():
    with pytest.raises(SdsInvalidTargetBucketForLogging) as exc_info:
        raise SdsInvalidTargetBucketForLogging(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidTargetBucketForLogging"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_invalid_token_exception():
    with pytest.raises(SdsInvalidToken) as exc_info:
        raise SdsInvalidToken(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidToken"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_invalid_uri_exception():
    with pytest.raises(SdsInvalidURI) as exc_info:
        raise SdsInvalidURI(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "InvalidURI"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_key_too_long_exception():
    with pytest.raises(SdsKeyTooLong) as exc_info:
        raise SdsKeyTooLong(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "KeyTooLong"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_malformed_acl_error_exception():
    with pytest.raises(SdsMalformedACLError) as exc_info:
        raise SdsMalformedACLError(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "MalformedACLError"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_malformed_post_request_exception():
    with pytest.raises(SdsMalformedPOSTRequest) as exc_info:
        raise SdsMalformedPOSTRequest(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "MalformedPOSTRequest"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_malformed_xml_exception():
    with pytest.raises(SdsMalformedXML) as exc_info:
        raise SdsMalformedXML(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "MalformedXML"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_max_message_length_exceeded_exception():
    with pytest.raises(SdsMaxMessageLengthExceeded) as exc_info:
        raise SdsMaxMessageLengthExceeded(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "MaxMessageLengthExceeded"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_max_post_pre_data_length_exceeded_error_exception():
    with pytest.raises(SdsMaxPostPreDataLengthExceededError) as exc_info:
        raise SdsMaxPostPreDataLengthExceededError(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "MaxPostPreDataLengthExceededError"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_metadata_too_large_exception():
    with pytest.raises(SdsMetadataTooLarge) as exc_info:
        raise SdsMetadataTooLarge(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "MetadataTooLarge"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_method_not_allowed_exception():
    with pytest.raises(SdsMethodNotAllowed) as exc_info:
        raise SdsMethodNotAllowed(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "MethodNotAllowed"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 405


def test_missing_attachment_exception():
    with pytest.raises(SdsMissingAttachment) as exc_info:
        raise SdsMissingAttachment(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "MissingAttachment"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code is None


def test_missing_content_length_exception():
    with pytest.raises(SdsMissingContentLength) as exc_info:
        raise SdsMissingContentLength(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "MissingContentLength"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 411


def test_missing_request_body_error_exception():
    with pytest.raises(SdsMissingRequestBodyError) as exc_info:
        raise SdsMissingRequestBodyError(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "MissingRequestBodyError"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_missing_security_element_exception():
    with pytest.raises(SdsMissingSecurityElement) as exc_info:
        raise SdsMissingSecurityElement(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "MissingSecurityElement"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_missing_security_header_exception():
    with pytest.raises(SdsMissingSecurityHeader) as exc_info:
        raise SdsMissingSecurityHeader(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "MissingSecurityHeader"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_no_logging_status_for_key_exception():
    with pytest.raises(SdsNoLoggingStatusForKey) as exc_info:
        raise SdsNoLoggingStatusForKey(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "NoLoggingStatusForKey"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_no_such_bucket_exception():
    with pytest.raises(SdsNoSuchBucket) as exc_info:
        raise SdsNoSuchBucket(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "NoSuchBucket"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 404


def test_no_such_key_exception():
    with pytest.raises(SdsNoSuchKey) as exc_info:
        raise SdsNoSuchKey(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "NoSuchKey"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 404


def test_no_such_lifecycle_configuration_exception():
    with pytest.raises(SdsNoSuchLifecycleConfiguration) as exc_info:
        raise SdsNoSuchLifecycleConfiguration(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "NoSuchLifecycleConfiguration"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 404


def test_no_such_upload_exception():
    with pytest.raises(SdsNoSuchUpload) as exc_info:
        raise SdsNoSuchUpload(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "NoSuchUpload"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 404


def test_no_such_version_exception():
    with pytest.raises(SdsNoSuchVersion) as exc_info:
        raise SdsNoSuchVersion(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "NoSuchVersion"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 404


def test_not_implemented_exception():
    with pytest.raises(SdsNotImplemented) as exc_info:
        raise SdsNotImplemented(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "NotImplemented"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 501


def test_not_signed_up_exception():
    with pytest.raises(SdsNotSignedUp) as exc_info:
        raise SdsNotSignedUp(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "NotSignedUp"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 403


def test_no_such_bucket_policy_exception():
    with pytest.raises(SdsNoSuchBucketPolicy) as exc_info:
        raise SdsNoSuchBucketPolicy(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "NoSuchBucketPolicy"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 404


def test_operation_aborted_exception():
    with pytest.raises(SdsOperationAborted) as exc_info:
        raise SdsOperationAborted(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "OperationAborted"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 409


def test_permanent_redirect_exception():
    with pytest.raises(SdsPermanentRedirect) as exc_info:
        raise SdsPermanentRedirect(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "PermanentRedirect"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 301


def test_precondition_failed_exception():
    with pytest.raises(SdsPreconditionFailed) as exc_info:
        raise SdsPreconditionFailed(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "PreconditionFailed"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 412


def test_redirect_exception():
    with pytest.raises(SdsRedirect) as exc_info:
        raise SdsRedirect(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "Redirect"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 307


def test_restore_already_in_progress_exception():
    with pytest.raises(SdsRestoreAlreadyInProgress) as exc_info:
        raise SdsRestoreAlreadyInProgress(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "RestoreAlreadyInProgress"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 409


def test_request_is_not_multi_part_content_exception():
    with pytest.raises(SdsRequestIsNotMultiPartContent) as exc_info:
        raise SdsRequestIsNotMultiPartContent(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "RequestIsNotMultiPartContent"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_request_timeout_exception():
    with pytest.raises(SdsRequestTimeout) as exc_info:
        raise SdsRequestTimeout(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "RequestTimeout"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_request_time_too_skewed_exception():
    with pytest.raises(SdsRequestTimeTooSkewed) as exc_info:
        raise SdsRequestTimeTooSkewed(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "RequestTimeTooSkewed"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 403


def test_request_torrent_of_bucket_error_exception():
    with pytest.raises(SdsRequestTorrentOfBucketError) as exc_info:
        raise SdsRequestTorrentOfBucketError(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "RequestTorrentOfBucketError"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_signature_does_not_match_exception():
    with pytest.raises(SdsSignatureDoesNotMatch) as exc_info:
        raise SdsSignatureDoesNotMatch(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "SignatureDoesNotMatch"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 403


def test_service_unavailable_exception():
    with pytest.raises(SdsServiceUnavailable) as exc_info:
        raise SdsServiceUnavailable(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "ServiceUnavailable"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 503


def test_slow_down_exception():
    with pytest.raises(SdsSlowDown) as exc_info:
        raise SdsSlowDown(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "SlowDown"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 503


def test_temporary_redirect_exception():
    with pytest.raises(SdsTemporaryRedirect) as exc_info:
        raise SdsTemporaryRedirect(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "TemporaryRedirect"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 307


def test_token_refresh_required_exception():
    with pytest.raises(SdsTokenRefreshRequired) as exc_info:
        raise SdsTokenRefreshRequired(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "TokenRefreshRequired"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_too_many_buckets_exception():
    with pytest.raises(SdsTooManyBuckets) as exc_info:
        raise SdsTooManyBuckets(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "TooManyBuckets"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_unexpected_content_exception():
    with pytest.raises(SdsUnexpectedContent) as exc_info:
        raise SdsUnexpectedContent(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "UnexpectedContent"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_unresolvable_grant_by_email_address_exception():
    with pytest.raises(SdsUnresolvableGrantByEmailAddress) as exc_info:
        raise SdsUnresolvableGrantByEmailAddress(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "UnresolvableGrantByEmailAddress"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400


def test_user_key_must_be_specified_exception():
    with pytest.raises(SdsUserKeyMustBeSpecified) as exc_info:
        raise SdsUserKeyMustBeSpecified(
            message="UnknownMessage",
            request_id="UnknownRequestId",
            resource="UnknownResource"
        )
    assert exc_info.value.code == "UserKeyMustBeSpecified"
    assert exc_info.value.message == "UnknownMessage"
    assert exc_info.value.request_id == "UnknownRequestId"
    assert exc_info.value.resource == "UnknownResource"
    assert exc_info.value.status_code == 400
