# -*- coding: utf-8 -*-

from lightningwolf_sds.core.errors import CoreException
from lightningwolf_sds.core.responses import error_response
from mock import patch


def test_error_repsonse():
    code = 'TestError'
    message = 'This is Not Implemented!'
    request_id = 'TestRequestId'
    resource = 'Unknown'
    status_code = 500
    e = CoreException(code=code, message=message, request_id=request_id, resource=resource, status_code=status_code)
    xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Error>{}</Error>".format(
        "<Code>{}</Code><Message>{}</Message><Resource>{}</Resource><RequestId>{}</RequestId>".format(
            code, message, resource, request_id
        )
    )
    with patch('lightningwolf_sds.core.responses.Response') as response:
        error_response(e)
        response.assert_called_with(response=xml, status=e.get_status_code(), mimetype='application/xml')
