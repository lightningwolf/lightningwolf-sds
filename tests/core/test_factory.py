# -*- coding: utf-8 -*-

import pytest


@pytest.mark.options(debug=False)
@pytest.mark.xfail()
def test_app(app):
    assert app.port == 5000, 'Ensure that app default port is 5000'
    assert not app.debug, 'Ensure the app not in debug mode'
