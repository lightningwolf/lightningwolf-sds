# -*- coding: utf-8 -*-

from click.testing import CliRunner

from lightningwolf_sds import cli
from mock import call, patch


class TestLightningwolfSdSCli(object):

    @classmethod
    def setup_class(cls):
        pass

    def test_something(self):
        pass

    def test_command_line_interface(self):
        with patch('lightningwolf_sds.cli.app') as app:
            runner = CliRunner()
            result = runner.invoke(cli.main)
            assert app.run.call_args == call(host='0.0.0.0')
            assert result.exit_code == 0
            assert 'Starting service' in result.output
            help_result = runner.invoke(cli.main, ['--help'])
            assert help_result.exit_code == 0
            assert '--help  Show this message and exit.' in help_result.output

    @classmethod
    def teardown_class(cls):
        pass
