#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from lightningwolf_sds.core.connection import SDSConnection
from lightningwolf_sds.core.factory import create_app


@pytest.fixture
def app():
    application = create_app()
    return application


@pytest.fixture(scope='function')
def sds_conn():
    conn = SDSConnection(
        'aws_access_key_id', 'aws_secret_access_key', host="dev-sds.lightningwolf.pl", port=5000, is_secure=False
    )
    return conn
