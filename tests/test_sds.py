# -*- coding: utf-8 -*-

from unittest import TestCase

from lightningwolf_sds import sds


class TestLightningwolfSdS(TestCase):

    def setUp(self):
        sds.app.config['TESTING'] = True
        self.app = sds.app.test_client()
        # with sds.app.app_context():
        #     sds.init_db()

    def test_hello_world(self):
        resp = self.app.get('/')
        assert resp.data == b'Hello World!'

    def tearDown(self):
        pass
