FROM python:3.6

ENV PYVER 3.6
ENV PYTHONUNBUFFERED 1
ENV GOSU_VERSION 1.10

RUN set -x \
    && apt-get update && apt-get install -y --no-install-recommends ca-certificates wget && rm -rf /var/lib/apt/lists/* \
    && dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')" \
    && wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch" \
    && wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch.asc" \
    && export GNUPGHOME="$(mktemp -d)" \
    && gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 \
    && gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu \
    && rm -r "$GNUPGHOME" /usr/local/bin/gosu.asc \
    && chmod +x /usr/local/bin/gosu \
    && gosu nobody true

COPY docker-entrypoint.sh /

RUN set -x \
  && addgroup --gid 1100 uwsgi \
  && adduser --disabled-password --disabled-login --gecos '' --uid 1100 --gid 1100 --home /app uwsgi

COPY dist /dist

RUN pip install /dist/lightningwolf_sds-0.1.0.tar.gz

WORKDIR /app

EXPOSE 9090 9191

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["uwsgi"]
