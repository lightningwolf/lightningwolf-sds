=================
Lightningwolf SDS
=================


.. image:: https://img.shields.io/pypi/v/lightningwolf_sds.svg
        :target: https://pypi.python.org/pypi/lightningwolf_sds

.. image:: https://gitlab.com/lightningwolf/lightningwolf-sds/badges/master/build.svg
        :target: https://gitlab.com/lightningwolf/lightningwolf-sds/commits/master

.. image:: https://gitlab.com/lightningwolf/lightningwolf-sds/badges/master/coverage.svg
        :target: https://gitlab.com/lightningwolf/lightningwolf-sds/commits/master

.. image:: https://gitpitch.com/assets/badge.svg
   :target: https://gitpitch.com/lightningwolf/lightningwolf-sds/master?grs=gitlab&t=white


* Free software: MIT license
* Repository: https://gitlab.com/lightningwolf/lightningwolf-sds
* Documentation: https://lightningwolf.gitlab.io/lightningwolf-sds/.


Foreword
--------

**Lightningwolf Simple DataStore** is a project created for two reasons:

*   To create interesting example based on something really succesfull 

For real use the best arternative in time I'm writting this is https://github.com/minio/minio project


Features
--------

* TODO


TODO
----

* Presentation (https://gitpitch.com/lightningwolf/lightningwolf-sds/master?grs=gitlab)
