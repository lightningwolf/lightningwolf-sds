#!/bin/bash
set -e

if [ "${1:0:1}" = '-' ]; then
    set -- gosu uwsgi /usr/local/bin/uwsgi \
        --wsgi-file "/usr/local/lib/python${PYVER}/site-packages/lightningwolf_sds/sds.py" "$@"
fi

if [ "$1" = 'uwsgi' -a "$(id -u)" = '0' ]; then

    set -- gosu uwsgi /usr/local/bin/uwsgi --http 0.0.0.0:9090 \
        --wsgi-file "/usr/local/lib/python${PYVER}/site-packages/lightningwolf_sds/sds.py" \
        --callable app --stats 0.0.0.0:9191

fi

# Allow the user to run arbitrarily commands like bash
exec "$@"
