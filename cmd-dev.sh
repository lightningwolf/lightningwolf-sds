#!/usr/bin/env bash
set -e

if [ "$ENV" = 'DEV' ]; then
    echo "Running Development Server"
    exec gosu uwsgi python "lightningwolf_sds/sds.py"
elif [ "$ENV" = 'UNIT' ]; then
    echo "Running Unit Tests"
    exec gosu uwsgi /app/.local/bin/py.test /app
else
    echo "Running Production Server"
    exec gosu uwsgi /app/.local/bin/uwsgi --http 0.0.0.0:9090 \
        --wsgi-file "/usr/local/lib/python${PYVER}/site-packages/lightningwolf_sds/sds.py" \
        --callable app --stats 0.0.0.0:9191
fi
