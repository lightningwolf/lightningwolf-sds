#!/bin/bash
set -e

# Allow DEV and UNIT preparations
if [ "$ENV" = 'DEV' ]; then
    gosu uwsgi pip install --user -r requirements/development.txt
    gosu uwsgi pip install --user -e .
elif [ "$ENV" = 'UNIT' ]; then
    gosu uwsgi pip install --user -r requirements/test.txt
    gosu uwsgi pip install --user -e .
elif [ "$ENV" = 'PROD' ]; then
    gosu uwsgi pip install --user -r requirements/main.txt
    gosu uwsgi pip install --user -e .
fi

# Allow the user to run arbitrarily commands like bash
exec "$@"
